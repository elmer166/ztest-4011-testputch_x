#include "lcd.h"

int fputc ( int ch, int f )
{
  switch (ch)
    {
    case 0x01: case 0x02: case 0x03: case 0x04: case 0x05: case 0x06:
    case 0x07: case 0x09: case 0x0d: case 0x0e: case 0x0f: case 0x10:
    case 0x11: case 0x12: case 0x13: case 0x14: case 0x15: case 0x16:
    case 0x17: case 0x18: case 0x19: case 0x1d: case 0x1e: case 0x1f:
      break;
    case 0x08:      // backspace
      LCDleft();
      break;
    case 0x0c:      // New page
      LCDclear();   // fall though to home
    case 0x0b:      // Home (Actualy a VT but close)
      LCDposition( 0 );
      break;
    case 0x0a:      // newline, actually only goes to line 2
      LCDline2();
      break;
    default:
      LCDletter(ch);
    }
  return 1;
}
