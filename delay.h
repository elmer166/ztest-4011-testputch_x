/*! \file delay.h
 *
 *   \brief Declarations for LCD delay routines
 */
//#define Fcy  14754600
//! Instruction clock Hz
// Original, presumably PIC24F
//#define Fcy  16000000
// dsPIC33 at full bore
#define Fcy  30000000

//! Delay for a specific count
void Delay( unsigned int delay_count );
//! Delay for a specified number of microseconds
void Delay_Us( unsigned int delayUs_count );

//! Counts for a 200 us delay
// Rather substantial rounding errors contribute to the
// differences in divisor - these values produce results
// less than 10% too long (but always long) for a
// 40MHz clock.
//#define Delay200uS_count  (Fcy * 0.0002) / 1080
#define Delay200uS_count  (Fcy * 0.0002) / 2600
//! Counts for a 1 ms delay
//#define Delay_1mS_Cnt	  (Fcy * 0.001) / 2950
#define Delay_1mS_Cnt	  (Fcy * 0.001) / 5700
//! Counts for a 2 ms delay
//#define Delay_2mS_Cnt	  (Fcy * 0.002) / 2950
#define Delay_2mS_Cnt	  (Fcy * 0.002) / 5900
//! Counts for a 5 ms delay
//#define Delay_5mS_Cnt	  (Fcy * 0.005) / 2950
#define Delay_5mS_Cnt	  (Fcy * 0.005) / 6200
//! Counts for a 15 ms delay
//#define Delay_15mS_Cnt 	  (Fcy * 0.015) / 2950
#define Delay_15mS_Cnt 	  (Fcy * 0.015) / 6382
//! Counts for a 1 second delay
//#define Delay_1S_Cnt	  (Fcy * 1) / 2950
#define Delay_1S_Cnt	  (Fcy * 1) / 6224

