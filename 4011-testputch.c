/* \file 4011-testputch.c
 *
 * \brief
 *
 *
 * Author: jjmcd
 *
 * Created on September 8, 2012, 3:54 PM
 */

/******************************************************************************
 * Software License Agreement
 *
 * Copyright (c) 2012 by John J. McDonough, WB8RCR
 *
 * This software is released under the GNU General Public License V2.
 * See the file COPYING for a complete description.
 *
 *****************************************************************************/


#if defined(__PIC24E__) 
#include <p24Exxxx.h>

#elif defined (__PIC24F__) 
#include <p24Fxxxx.h>

#elif defined(__PIC24H__)
#include <p24Hxxxx.h>

#elif defined(__dsPIC30F__)
#include <p30Fxxxx.h>

#elif defined (__dsPIC33E__) 
#include <p33Exxxx.h>

#elif defined(__dsPIC33F__) 
#include <p33Fxxxx.h>

#endif

#include <stdio.h>
#include "lcd.h"
#include "delay.h"

// Configuration fuses
_FOSC(XT_PLL16 & PRI)                   // 7.3728 rock * 16 = 118MHz
_FWDT(WDT_OFF)                          // Watchdog timer off
_FBORPOR(PWRT_16 & PBOR_OFF & MCLR_EN)  // Brownout off, powerup 16ms
_FGS(GWRP_OFF & CODE_PROT_OFF)          // No code protection

//! main - Exercise printf

int main (void)
{
  int j;
  float f;

  LCDinit();
  LCDclear();
  puts("Testing fputc()\nroutine for LCD");

  // Initialize some values for display
  j = 3;
  f = 1.37;
  
  do
    {
      Delay(Delay_1S_Cnt);

      puts("\fShould be a\nscreen clear");
      Delay(Delay_1S_Cnt);
      puts("\f");
      Delay(Delay_1S_Cnt);
      puts("\vcleared?");
      Delay(Delay_1S_Cnt);
      puts("\f\nTesting home");
      Delay(Delay_1S_Cnt);
      puts("\vhome?");
      Delay(Delay_1S_Cnt);
      puts("\fBack 3\244b\244b2\244b\244b1\n      3\b\b2\b\b1");
      Delay(Delay_1S_Cnt);
      Delay(Delay_1S_Cnt);

      // Get some values to display
      j = j * 7;
      if ( j < -100 )
        j = j & 7;
      f = f * 14.38;
      if ( f > 1.0e20 )
        f = 3.74e-8;
/*
      printf("\f\nPlease note..");
      Delay(Delay_1S_Cnt);
      Delay(Delay_1S_Cnt);
      printf("\f\vPlease note..\nthe backslash");
      Delay(Delay_1S_Cnt);
      Delay(Delay_1S_Cnt);
      printf("\f\vthe backslash\ncharacter on all");
      Delay(Delay_1S_Cnt);
      Delay(Delay_1S_Cnt);
      printf("\f\vcharacter on all\nLCDs shows as a");
      Delay(Delay_1S_Cnt);
      Delay(Delay_1S_Cnt);
      printf("\f\vLCDs shows as a\nYen symbol so a");
      Delay(Delay_1S_Cnt);
      Delay(Delay_1S_Cnt);
      printf("\f\vYen symbol so a\nvertical bar is");
      Delay(Delay_1S_Cnt);
      Delay(Delay_1S_Cnt);
      printf("\f\vvertical bar is\nshown instead.");
      Delay(Delay_1S_Cnt);
      Delay(Delay_1S_Cnt);
      printf("\f\vshown instead.");
      Delay(Delay_1S_Cnt);
      Delay(Delay_1S_Cnt);
      printf("\f\v");
      Delay(Delay_1S_Cnt);
      Delay(Delay_1S_Cnt);
*/

      // Now try a few formats
      printf("\fi = %d\nf=%g",j,f);
      Delay(Delay_1S_Cnt);

      puts("\fNow formatted..\nprintf(\"...");
      Delay(Delay_1S_Cnt);

      printf("\f\244nf=[%%12.4g]\nf=[%12.4g]",f);
      Delay(Delay_1S_Cnt);

      printf("\f\244nf=[%%12.4f]\nf=[%12.4f]",f);
      Delay(Delay_1S_Cnt);

      printf("\f\244nf=[%%10.4E]\nf=[%10.4E]",f);
      Delay(Delay_1S_Cnt);

      printf("\f\244nf=[%%10e]\nf=[%10e]",f);
      Delay(Delay_1S_Cnt);
      
    } while(1);
}
